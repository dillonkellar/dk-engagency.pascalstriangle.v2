using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Pascals_Triangle
{
    class Program
    {
        private const int RowsToGenerate = 30;
        private const string OutputFileLocation = @"C:\tmp\pascal.txt";

        static void Main(string[] args)
        {
            PascalTriangle triangle = new PascalTriangle(RowsToGenerate);
            triangle.Fill();
            using (var writer = new StreamWriter(OutputFileLocation))
            {
                foreach (var row in triangle.TriangleMatrix)
                {
                    writer.WriteLine(string.Join(" ", row));
                }
            }
        }
    }

    public class PascalTriangle
    {
        // Use matrix to represent values in the triangle so that each value is only calculated once
        public List<List<int>> TriangleMatrix = new List<List<int>>();

        public PascalTriangle(int rows)
        {
            for(int i = 0; i < rows; i++)
            {
                // Create each row so that SetCellValue does not have to create non-existent rows
                TriangleMatrix.Add(new List<int>());
            }
            TriangleMatrix[0].Add(1);
        }

        // Fills the triangle matrix
        public void Fill()
        {
            int rows = TriangleMatrix.Count();
            for(int row = 1; row < rows; row++)
            {
                int columns = row + 1;
                for (int column = 0; column < columns; column++)
                {
                    SetCellValue(row, column);
                }
            }
        }

        public int GetCellValue(int row, int column)
        {
            // Edges of the triangle are always 1
            // Don't need to check if the row is 0 because when it is the column is also 0
            if (column == 0 || row == column) return 1;
            else return TriangleMatrix[row - 1][column - 1] + TriangleMatrix[row - 1][column];
        }

        public void SetCellValue(int row, int column)
        {
            int value = GetCellValue(row, column);
            TriangleMatrix[row].Add(value);
        }
    }
}